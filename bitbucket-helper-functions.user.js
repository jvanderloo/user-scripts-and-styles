// ==UserScript==
// @name     Bitbucket Cloud Helper Functions
// @version  1
// @grant    none
// @match  	 https://bitbucket.org/*
// @author   John van der Loo <john.vanderloo@pathzero.com>
// ==/UserScript==

(function() {

  function isPullRequestPage() {
    return window.location.pathname.match(/pathzero\/.*\/pull-requests/);
  }

  function isFilesTabActive() {
    const filesTab = document.querySelector('#bb-sidebar [data-testid="sidebar-tab-files"]');
    return filesTab.matches('[aria-selected="true"]');
  }

  /**
   * When a file is clicked on the PR page, update the window hash to point to that file.
   */
  function handleFileClicks(e) {
    if (!isPullRequestPage() || !isFilesTabActive()) {
      return;
    }

    if (e.target.matches('#bb-sidebar a[href], #bb-sidebar a[href] span')) {
      const el = e.target.closest('a');

      window.location.hash = el.getAttribute('href');
    }
  }

  /**
   * When navigating the PR file tree, if there is a generated client, automagically collapse it
   */
  function collapseGeneratedClientDir() {
    if (!isPullRequestPage() || !isFilesTabActive()) {
      return;
    }

    // don't collapse if we're browsing inside that dir
    if (window.location.hash.includes('generated-client')) {
        return;
    }

    const generatedClientEl = Array.from(document.querySelectorAll('#bb-sidebar li button span div span')).find(el => el.textContent === 'generated-client')

    // If a generated-client dir exists, collapse it.
    if (generatedClientEl) {
      generatedClientEl.closest('button').click();
    }

  }

  window.addEventListener('hashchange', collapseGeneratedClientDir);
  document.addEventListener('click', handleFileClicks);

}());